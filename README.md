This repository holds information about the dataset used in the following paper that appeared in the proceedings of ACM SIGSPATIAL 2018.
```
  Fusion of Aerial Lidar and Images for Road Segmentation with Deep CNN
  Biswas Parajuli, Piyush Kumar, Tathagata Mukherjee, Eduardo Pasiliao, Sachin Jambawalikar
```

Citation (BibTex)
=================
```
@inproceedings{parajuli2018fusion,
  title={Fusion of aerial lidar and images for road segmentation with deep cnn},
  author={Parajuli, Biswas and Kumar, Piyush and Mukherjee, Tathagata and Pasiliao, Eduardo and Jambawalikar, Sachin},
  booktitle={Proceedings of the 26th ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems},
  pages={548--551},
  year={2018}
}
```

Dataset URL
===========
The dataset can be downloaded from the following link
```
  http://ww2.cs.fsu.edu/~parajuli/datasets/fusion_lidar_images_sigspatial18.zip
```

Dataset Description
===================
It includes RGB images, depth images and road masks for 5860 tiles.
The depth images are produced by the Lidar Processing Unit described in the paper.
There are four text files, namely, `train.txt`, `valid.txt`, `test.txt` and `view.txt`
which hold the sets of tiles used for training, validation, testing and visualization respectively.
Each tile has a resolution of 500x500 pixels.
`view.txt` holds 400 contiguous tiles which form a 10000x10000 resolution tile when combined.

The size of the dataset when uncompressed is as follows:
```
  RGB images: 2.6 GB
  Depth images: 564 MB
  Road masks: 24 MB
```

Questions
=========

###### Why are all the images in the mask directory black?

Those are binary masks. They can appear to be black if they are loaded as grayscale images. You can load them as follows:

```python
>>> import cv2
>>> import numpy as np
>>> fn = "mask/48841_0_13.png"
>>> a = cv2.imread(fn, cv2.IMREAD_UNCHANGED)
>>> a.shape
(500, 500)
>>> np.max(a)
1
```

